package ml.inobmabiki4.issues.unusedparameter

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import ml.inobmabiki4.issues.unusedparameter.ui.theme.UnusedParameterInComposableTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            UnusedParameterInComposableTheme {
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background,
                ) {
                    Greeting("Leland Richardson")
                }
            }
        }
    }
}

// Hey there! If you open me in Android Studio,
// then go to `Problems > Project Errors > Inspect Code...`
// and `Analyze` `Whole Project` using the `Project Default` inspection profile,
// you will see some false positive warnings.

@Composable
fun Greeting(name: String) { // Parameter 'name' is deemed unused,
    Surface {
        Text(text = "Hello, $name!") // but 'name' is used here!
    }
}

fun outer(parameter: String) { // Parameter 'parameter' is deemed unused,
    fun inner() {
        print(parameter) // but 'parameter' is used here!
    }
    inner()
}

fun main() {
    outer("Some Value")
}
